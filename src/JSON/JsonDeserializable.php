<?php

declare(strict_types=1);

namespace Tools\JSON;

use stdClass;

/**
 * Interface JsonDeserializable
 * Marks class that can be initialized from json data
 *
 * @package JSONAPI
 */
interface JsonDeserializable
{
    /**
     * @param stdClass|array $json
     *
     * @return static
     */
    public static function jsonDeserialize($json): self;
}
