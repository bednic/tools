<?php

declare(strict_types=1);

namespace Tools\JSON;

/**
 * Interface JsonSerializable
 *
 * @package Tools\JSON
 */
interface JsonSerializable extends \JsonSerializable
{

}
