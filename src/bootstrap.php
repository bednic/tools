<?php

$functions = new DirectoryIterator(__DIR__ . DIRECTORY_SEPARATOR . 'functions');
foreach ($functions as $function) {
    if ($function->isFile()) {
        include_once $function->getRealPath();
    }
}
