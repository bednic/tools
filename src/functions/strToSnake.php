<?php

declare(strict_types=1);

if (!function_exists('strToSnake')) {
    function strToSnake(string $string): string
    {
        preg_match_all('/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/', $string, $matches);
        return implode('_', array_map(fn($item) => strtolower($item), $matches[0]));
    }
}
