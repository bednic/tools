<?php

declare(strict_types=1);

if (!function_exists('strToCamel')) {
    function strToCamel(string $string): string
    {
        preg_match_all('/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/', $string, $matches);
        return lcfirst(implode('', array_map(fn($item) => ucfirst($item), $matches[0])));
    }
}
