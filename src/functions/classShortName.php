<?php

if (!function_exists('classShortName')) {
    function classShortName(string $string): string
    {
        preg_match('/[A-Za-z0-9_]+$/', $string, $match);
        return $match[0] ?? $string;
    }
}
