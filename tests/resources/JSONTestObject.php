<?php

declare(strict_types=1);

namespace Tools\Test\resources;

use Tools\JSON\JsonConvertible;

class JSONTestObject implements JsonConvertible
{

    private string $key = 'value';


    public static function jsonDeserialize($json): self
    {
        $instance = new self();
        if (is_array($json)) {
            $instance->key = $json['key'];
        } else {
            $instance->key = $json->key;
        }
        return $instance;
    }

    public function jsonSerialize()
    {
        return ['key' => $this->key];
    }
}
