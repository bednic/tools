# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
* added some new universal string conversion functions:
    * strToCamel
    * strToKebab
    * strToPascal
    * strToSnake

### Changed

### Deprecated

### Fixed

### Removed
* camelToKebab deprecated by strToKebab
* camelToSnake deprecated by strToSnake

### Security

## [0.3.0]

### Added
* JSON utils:
    * JsonSerializable interface
    * JsonDeserializable interface
    * JsonConvertible interface

### Changed

### Deprecated

### Fixed

### Removed

### Security

## [0.2.1]

### Added

### Changed

### Deprecated

### Fixed
* bootstrap functions loading

### Removed

### Security

## [0.2.0]

### Added
* `classShortName` function - clear namespace from class name and return short class name

### Changed

### Deprecated

### Fixed

### Removed

### Security

## [0.1.0]

### Added
* `camelToSnake` function do from `camelCase` this `camel_case`

### Changed
* separate function to files, per function

### Deprecated

### Fixed

### Removed

### Security

## [0.0.1]

### Added
* camelToKebab - change camelCase to kebab-case
* slashToDot - replace slashes in full class name to dots, can be used e.g. for directory names
* pluralize - pluralize word, e.g. proxy => proxies, variable => variables
* formatBytes - format Bytes to kB, MB, GB, TB
* floatValue - format string to valid float, e.g. 123,132.123 or 123.123,123 both ends as 123123.123

### Changed

### Deprecated

### Fixed

### Removed

### Security


---
[Unreleased]: https://gitlab.com/bednic/json-api/compare/0.2.1...master
[0.2.1]: https://gitlab.com/bednic/json-api/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/bednic/json-api/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/bednic/json-api/compare/0.0.1...0.1.0
[0.0.1]: https://gitlab.com/bednic/json-api/compare/0.0.1...0.0.1
